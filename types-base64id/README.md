# Installation
> `npm install --save @types/base64id`

# Summary
This package contains type definitions for base64id (https://github.com/faeldt/base64id).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/base64id.

### Additional Details
 * Last updated: Tue, 10 Nov 2020 22:14:38 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Shadman Kolahzary](https://github.com/Kolahzary).
